# EASY

# Return the argument with all its lowercase characters removed.
def destructive_uppercase(str)

  str.chars.select { |char| char != char.downcase }.join

end

# Return the middle character of a string. Return the middle two characters if
# the word is of even length, e.g. middle_substring("middle") => "dd",
# middle_substring("mid") => "i"
def middle_substring(str)

  if str.length.odd?
    str[str.length/2]
  else
    str[str.length/2-1..str.length/2]
  end

end

# Return the number of vowels in a string.
VOWELS = %w(a e i o u)
def num_vowels(str)

  str.chars.count { |char| VOWELS.include?(char)}

end

# Return the factoral of the argument (num). A number's factorial is the product
# of all whole numbers between 1 and the number itself. Assume the argument will
# be > 0.
def factorial(num)

  (1..num).reduce(:*)

end


# MEDIUM

# Write your own version of the join method. separator = "" ensures that the
# default seperator is an empty string.
def my_join(arr, separator = "")
  answer = ""

  arr.each do |element|

    if element != arr.last
      answer += element + separator
    else
      answer += element
    end

  end

  answer

end

# Write a method that converts its argument to weirdcase, where every odd
# character is lowercase and every even is uppercase, e.g.
# weirdcase("weirdcase") => "wEiRdCaSe"
def weirdcase(str)
  str.downcase!

  str.chars.each_index do |idx|

    str[idx] = str[idx].upcase! if idx.odd?
  end

  str
end

# Reverse all words of five more more letters in a string. Return the resulting
# string, e.g., reverse_five("Looks like my luck has reversed") => "skooL like
# my luck has desrever")
def reverse_five(str)

  str_split = str.split(" ")

  str_split.map! do |word|
    if word.length >= 5
      word.reverse
    else
      word
    end
  end

  str_split.join(" ")

end

# Return an array of integers from 1 to n (inclusive), except for each multiple
# of 3 replace the integer with "fizz", for each multiple of 5 replace the
# integer with "buzz", and for each multiple of both 3 and 5, replace the
# integer with "fizzbuzz".
def fizzbuzz(n)

  (1..n).to_a.map! do |number|

    if number % 3 == 0 && number % 5 == 0
      number = "fizzbuzz"
    elsif number % 5 == 0
      number = "buzz"
    elsif number % 3 == 0
      number = "fizz"
    else
      number
    end
  end

end


# HARD

# Write a method that returns a new array containing all the elements of the
# original array in reverse order.
def my_reverse(arr)
  answer = []

  i = arr.length - 1

  while i >= 0
    answer << arr[i]
    i -= 1
  end

  answer


end

# Write a method that returns a boolean indicating whether the argument is
# prime.
def prime?(num)

  return false if num == 1

  (2...num).none? { |number| num % number == 0 }

end

# Write a method that returns a sorted array of the factors of its argument.
def factors(num)

  (1..num).select { |number| num % number == 0 }

end

# Write a method that returns a sorted array of the prime factors of its argument.
def prime_factors(num)

  (2..num).select { |number| prime?(number) && num % number == 0 }

end

# Write a method that returns the number of prime factors of its argument.
def num_prime_factors(num)

  prime_factors(num).count

end


# EXPERT

# Return the one integer in an array that is even or odd while the rest are of
# opposite parity, e.g. oddball([1,2,3]) => 2, oddball([2,4,5,6] => 5)
def oddball(arr)

  even = arr.select(&:even?)
  odd = arr.select(&:odd?)

  even.count == 1 ? even.first : odd.first

end
